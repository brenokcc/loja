Djangoplus requires the library "Pillow==3.2.0". If you are using Linux, please execute the following command before installing the framework.

    sudo apt-get -y install libopenjpeg-dev libfreetype6-dev libtiff5-dev liblcms2-dev libwebp-dev tk8.6-dev libjpeg-dev



Afterwards, execute the following commands in the project root directory:

    mkvirtualenv djangoplus (optional)
    pip install -Ur requirements.txt
    python manage.py runserver



Run the automatic tests with the following command:
    python manage.py test