# -*- coding: utf-8 -*-

"""
Loja consiste em uma aplicação com dois perfis de usuários (gerene e vendedor) na qual é possível controlar o estoque e vendas de determinados produtos previamente cadastrados. O cadastro dos produtos e suas entradas no estoque são realizados pelo gerente. As vendas são realizdas pelos funcionários. Através do painel de controle é possível acompanhar o faturamento mensal, visualizar as vendas que ainda não foram finalizadas, entre outras funcionalidades.
"""