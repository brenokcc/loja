# -*- coding: utf-8 -*-

from django.conf import settings
from djangoplus.test import TestCase
from djangoplus.admin.models import User
from djangoplus.test.decorators import testcase


class AppTestCase(TestCase):

    def test(self):
        User.objects.create_superuser(settings.DEFAULT_SUPERUSER, None, settings.DEFAULT_PASSWORD)
        self.execute_flow()

    @testcase('Cadastrando Gerente')
    def cadastrar_gerente(self):
        self.click_menu('Cadastros', 'Funcionários', 'Gerentes')
        self.click_button('Cadastrar')
        self.enter('Nome', 'Juca da Silva')
        self.enter('CPF', '000.000.000-00')
        self.choose('Sexo', 'Masculino')
        self.enter('Data de Nascimento', '27/08/1984')
        self.enter('CEP', '59.141-000')
        self.enter('Número', '123')
        self.enter('Complemento', 'Apartamento 100')
        self.enter('Logradouro', 'Centro')
        self.enter('Bairro', 'Cohabinal')
        self.choose('Município', 'Parnamirim')
        self.click_button('Salvar')

    @testcase('Cadastrando Forma de Pagamento')
    def cadastrar_formapagamento(self):
        self.click_menu('Cadastros', 'Formas de Pagamento')
        self.click_button('Cadastrar')
        self.enter('Descrição', 'Dinheiro')
        self.click_button('Salvar')

    @testcase('Cadastrando Produto')
    def cadastrar_produto(self):
        self.click_link('Produtos')
        self.click_button('Cadastrar')
        self.enter('Preço Unitário', '23,00')
        self.enter('Descrição', 'Camisa')
        self.enter('Foto', '')
        self.click_button('Salvar')

    @testcase('Cadastrando Entrada de Produto')
    def cadastrar_entradaproduto(self):
        self.click_menu('Entrada de Produtos')
        self.click_button('Cadastrar')
        self.enter('Data', '01/01/2018')
        self.click_button('Salvar')

    @testcase('Adicionando Item à Entrada de Produto')
    def adicionar_itementrada_em_entradaproduto(self):
        self.click_menu('Entrada de Produtos')
        self.click_icon('Visualizar')
        self.look_at_panel('Itens')
        self.choose('Produto', 'Camisa')
        self.enter('Quantidade', '3')
        self.click_button('Adicionar')

    @testcase('Cadastrando Vendedor')
    def cadastrar_vendedor(self):
        self.click_menu('Cadastros', 'Funcionários', 'Vendedores')
        self.click_button('Cadastrar')
        self.enter('Nome', 'Juca da Silva')
        self.enter('CPF', '000.000.000-00')
        self.choose('Sexo', 'Masculino')
        self.enter('Data de Nascimento', '27/08/1984')
        self.enter('CEP', '59.141-000')
        self.enter('Número', '123')
        self.enter('Complemento', 'Apartamento 100')
        self.enter('Logradouro', 'Centro')
        self.enter('Bairro', 'Cohabinal')
        self.choose('Município', 'Parnamirim')
        self.click_button('Salvar')

    @testcase('Cadastrando Cliente')
    def cadastrar_cliente(self):
        self.click_link('Clientes')
        self.click_button('Cadastrar')
        self.enter('Nome', 'Juca da Silva')
        self.enter('CPF', '000.000.000-00')
        self.choose('Sexo', 'Masculino')
        self.enter('Data de Nascimento', '27/08/1984')
        self.enter('CEP', '59.141-000')
        self.enter('Número', '123')
        self.enter('Complemento', 'Apartamento 100')
        self.enter('Logradouro', 'Centro')
        self.enter('Bairro', 'Cohabinal')
        self.choose('Município', 'Parnamirim')
        self.click_button('Salvar')

    @testcase('Iniciando uma Venda')
    def iniciar_venda(self):
        self.click_link('Vendas')
        self.click_button('Iniciar Venda')
        self.choose('Cliente', 'Juca da Silva')
        self.enter('Data', '01/01/2018')
        self.click_button('Iniciar Venda')

    @testcase('Adicionando Item a uma Venda')
    def adicionar_itemvenda_em_venda(self):
        self.click_link('Vendas')
        self.click_icon('Visualizar')
        self.look_at_panel('Itens')
        self.choose('Produto', 'Camisa')
        self.enter('Quantidade', '1')
        self.click_button('Adicionar')

    @testcase('Finalizanod uma Venda')
    def finalizar_em_venda(self):
        self.click_link('Vendas')
        self.click_icon('Visualizar')
        self.click_button('Finalizar Venda')
        self.look_at_popup_window()
        self.choose('Forma de Pagamento', 'Dinheiro')
        self.enter('Valor Pago', '25,00')
        self.click_button('Finalizar Venda')