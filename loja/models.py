# -*- coding: utf-8 -*-

from djangoplus.db import models
from django.core.exceptions import ValidationError
from djangoplus.decorators import meta, action, subset, role
from pessoas.models import PessoaFisicaAbstrata


@role('cpf')
class Gerente(PessoaFisicaAbstrata):
    """
    Papel responsável por realizar os cadastros básicos e controle do estoque.
    """
    class Meta:
        verbose_name = 'Gerente'
        verbose_name_plural = 'Gerentes'
        menu = 'Cadastros::Funcionários::Gerentes', 'fa-th'
        usecase = 'UC001'


class FormaPagamento(models.Model):
    """
    Utilizada na finalização das vendas.
    """
    descricao = models.CharField(verbose_name='Descrição', example='Dinheiro')

    class Meta:
        verbose_name = 'Forma de Pagamento'
        verbose_name_plural = 'Formas de Pagamentos'
        menu = 'Cadastros::Formas de Pagamento', 'fa-th'
        can_admin = 'Gerente'
        usecase = 'UC002'

    def __str__(self):
        return self.descricao


class ProdutoManager(models.DefaultManager):

    @subset('Em estoque')
    def em_estoque(self):
        """
        Produtos disponíveis no estoque e que podem ser vendidos.
        """
        return self.exclude(id__in=self.indisponiveis())

    @subset('Indisponíveis')
    def indisponiveis(self):
        """
        Produtos indisponíveis no estoque e que não podem ser vendidos.
        """
        ids = []
        for produto in self.all():
            if not produto.get_estoque():
                ids.append(produto.pk)
        return self.filter(id__in=ids)

    @meta('Recém Vendidos', can_view='Vendedor', dashboard='center')
    def recem_vendidos(self):
        """
        Seis últimos produtos que foram vendidos.
        """
        ids = ItemVenda.objects.order_by('-venda__data').values_list('produto', flat=True)[0:6]
        return self.filter(id__in=ids)


class Produto(models.Model):
    foto = models.ImageField(verbose_name='Foto', upload_to='produtos', sizes=((300, 300),))
    descricao = models.TextField(verbose_name='Descrição', example='Camisa', search=True)
    valor = models.DecimalField(verbose_name='Preço Unitário', example='23,00')

    fieldsets = (
        ('Dados Gerais', {'image': 'foto', 'fields': ('valor', 'descricao')}),
        ('Estatísticas', {'extra': ('get_quantidade_vendida_por_periodo',)}),
    )

    class Meta:
        verbose_name = 'Produto'
        verbose_name_plural = 'Produtos'
        select_display = 'foto', 'descricao'
        menu = 'Cadastros::Produtos', 'fa-th'
        list_display = 'foto', 'descricao', 'valor', 'get_estoque'
        list_template = 'image_cards.html'
        list_shortcut = 'Gerente'
        can_admin = 'Gerente'
        can_list = 'Vendedor'
        icon = 'fa-archive'
        usecase = 'UC003'

    def __str__(self):
        return self.descricao

    @meta('Estoque', formatter='em_estoque')
    def get_estoque(self):
        entrada = ItemEntrada.objects.filter(produto=self).sum('quantidade')
        saida = ItemVenda.objects.filter(produto=self).sum('quantidade')
        return entrada - saida

    @meta('Quantidade Vendida por Período', formatter='line_chart')
    def get_quantidade_vendida_por_periodo(self):
        return ItemVenda.objects.filter(produto=self).sum('quantidade', 'venda__data')


class EntradaProduto(models.Model):
    """
    Registra a entrada de itens no estoque.
    """
    data = models.DateField(verbose_name='Data', example='01/01/2018')

    fieldsets = (
        ('Dados Gerais', {'fields': ('data',)}),
        ('Itens', {'inlines': ('itementrada_set',)}),
    )

    class Meta:
        verbose_name = 'Entrada de Produto'
        verbose_name_plural = 'Entradas de Produtos'
        verbose_female = True
        add_message = 'Por favor, informe os produtos adquiridos'
        menu = 'Entrada de Produtos', 'fa-sign-in'
        can_admin = 'Gerente'
        add_shortcut = True
        icon = 'fa-archive'
        usecase = 'UC004'


class ItemEntrada(models.Model):
    entrada = models.ForeignKey(EntradaProduto, verbose_name='Entrada', composition=2)
    produto = models.ForeignKey(Produto, verbose_name='Produto', example='Camisa')
    quantidade = models.IntegerField(verbose_name='Quantidade', example=3)

    class Meta:
        verbose_name = 'Item'
        verbose_name_plural = 'Itens'
        can_admin = 'Gerente'
        usecase = 'UC005'


@role('cpf')
class Vendedor(PessoaFisicaAbstrata):
    """
    Papel responsável por registrar as vendas.
    """
    class Meta:
        verbose_name = 'Vendedor'
        verbose_name_plural = 'Vendedores'
        menu = 'Cadastros::Funcionários::Vendedores', 'fa-th'
        can_admin = 'Gerente'
        usecase = 'UC006'


class Cliente(PessoaFisicaAbstrata):
    """
    Pessoas físicas compradoras dos produtos ofertados pela loja.
    """
    class Meta:
        verbose_name = 'Cliente'
        verbose_name_plural = 'Clientes'
        menu = 'Clientes', 'fa-users'
        list_shortcut = True
        can_admin = 'Vendedor'
        usecase = 'UC007'

    fieldsets = PessoaFisicaAbstrata.fieldsets + (('Outras Informações', {'extra': ('get_despesa_por_periodo',)}),)

    @meta('Despesa por Período', formatter='bar_chart')
    def get_despesa_por_periodo(self):
        return Venda.objects.filter(cliente=self).sum('valor_recebido', 'data')


class VendaManager(models.DefaultManager):

    @subset('Não-Finalizalidas', can_notify=True)
    def nao_finalizadas(self):
        """
        Vendas sem registros de pagamento.
        """
        return self.filter(forma_pagamento__isnull=True)

    @meta('Receita por Período', can_view='Gerente', formatter='line_chart', dashboard='center')
    def get_total_por_periodo(self):
        """
        Valor total das vendas por mês.
        """
        return self.sum('valor_recebido', 'data')

    @meta('Receita por Forma de Pagamento', formatter='chart', dashboard='right')
    def get_total_por_forma_pagamento(self):
        """
        Valor total das vendas por forma de pagamento.
        """
        return self.sum('valor_recebido', 'forma_pagamento')


class Venda(models.Model):
    cliente = models.ForeignKey(Cliente, verbose_name='Cliente', lazy=True, example='Juca da Silva', search=('cpf', 'nome'))
    data = models.DateField(verbose_name='Data', example='01/01/2018', filter=True)
    forma_pagamento = models.ForeignKey(FormaPagamento, verbose_name='Forma de Pagamento', filter=True, exclude=True, example='Dinheiro')
    valor_pago = models.DecimalField(verbose_name='Valor Pago', exclude=True, example='25,00')
    valor_recebido = models.DecimalField(verbose_name='Valor Recebido', exclude=True)
    troco = models.DecimalField(verbose_name='Troco', exclude=True)

    fieldsets = (
        ('Dados Gerais', {'fields': ('cliente', ('data', 'forma_pagamento'), ('valor_pago', 'valor_recebido', 'troco'))}),
        ('Itens', {'inlines': ('itemvenda_set',), 'actions': ('finalizar',)}),
    )

    class Meta:
        verbose_name = 'Venda'
        verbose_name_plural = 'Vendas'
        verbose_female = True
        menu = 'Vendas', 'fa-desktop'
        add_message = 'Por favor, informe os produtos adquiridos'
        add_shortcut = True
        list_shortcut = True
        icon = 'fa-desktop'
        add_label = 'Iniciar Venda'
        can_admin = 'Vendedor'
        usecase = 'UC008'
        class_diagram = 'venda', 'produto', 'itemvenda', 'formapagamento', 'cliente'

    @action('Finalizar Venda', can_execute='Vendedor', condition='not forma_pagamento', usecase='UC010')
    def finalizar(self, forma_pagamento, valor_pago):
        self.forma_pagamento = forma_pagamento
        self.valor_pago = valor_pago
        self.troco = self.valor_pago - self.get_total()
        if self.troco < 0:
            raise ValidationError('O valor pagamento deve ser maior ou igual ao valor total da venda')
        self.valor_recebido = self.valor_pago - self.troco
        self.save()

    @meta('Total')
    def get_total(self):
        return self.itemvenda_set.sum('subtotal')


class ItemVenda(models.Model):
    venda = models.ForeignKey(Venda, verbose_name='Venda', composition=True)
    produto = models.ForeignKey(Produto, verbose_name='Produto', example='Camisa')
    quantidade = models.IntegerField(verbose_name='Quantidade', example=1)
    subtotal = models.DecimalField(verbose_name='Subtotal', exclude=True)

    class Meta:
        verbose_name = 'Item de Venda'
        verbose_name_plural = 'Itens de Venda'
        list_total = 'subtotal'
        list_display = 'produto', 'produto__valor', 'quantidade', 'subtotal'
        can_admin = 'Vendedor'
        usecase = 'UC009'

    def save(self, *args, **kwargs):

        if not self.produto.get_estoque():
            raise ValidationError('Este produto não consta no estoque')

        self.subtotal = self.quantidade * self.produto.valor
        super(ItemVenda, self).save(*args, **kwargs)

    def can_add(self):
        return self.venda and not self.venda.forma_pagamento

    def can_edit(self):
        return self.can_add()

    def can_delete(self):
        return self.can_edit()




