# -*- coding: utf-8 -*-
from djangoplus.decorators.formatters import formatter


@formatter()
def em_estoque(value):
    return value and '{} em estoque'.format(value or 'Indisponível')

